#!C:\Strawberry\perl\bin\perl.exe -wT
use CGI;
use DBI;
use strict;
use warnings;

print CGI::header(-type => "application/json", -charset => "utf-8");

#read the CGI params
my $cgi = CGI->new;

# connect to the database
my $dbh = DBI->connect("DBI:mysql:store:localhost",  
  "root", "pass")
  or die $DBI::errstr;
# # check the username and password in the database
my $statement = qq{SELECT login_register.login_date, login_register.time_in, login_register.time_out, users.username FROM users join login_register on users.id = login_register.user_id};
my $sth = $dbh->prepare($statement)
  or die $dbh->errstr;
$sth->execute()
  or die $sth->errstr;
my @rows = ();
my @row;
while (@row = $sth->fetchrow_array) {  # retrieve one row
#print "ok";
    push @rows, "[\"".(join "\",\"", @row)."\"]";
}
print "[".(join ",", @rows)."]";