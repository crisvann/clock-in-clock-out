#!C:\Strawberry\perl\bin\perl.exe -wT
use CGI;
use DBI;
use strict;
use warnings;
# Object initialization:
print CGI::header(-type => "application/json", -charset => "utf-8");
#read the CGI params
my $cgi = CGI->new;
my $username = $cgi->param("username");
my $password = $cgi->param("password");

# connect to the database
my $dbh = DBI->connect("DBI:mysql:store:localhost",  
  "root", "Pass")
  or die $DBI::errstr;
# # check the username and password in the database
my $statement = qq{SELECT id FROM users WHERE username=? and password=?};
my $sth = $dbh->prepare($statement)
  or die $dbh->errstr;
$sth->execute($username, $password)
  or die $sth->errstr;
my ($userID) = $sth->fetchrow_array;
 
# create a JSON string according to the database result
my $json = ($userID) ? 
  qq{{"success" : "login is successful", "userid" : "$userID"}} : 
  qq{{"error" : "username or password is wrong"}};
 if($userID){
	my $ins_statement = qq{INSERT INTO login_register(login_date, time_in, user_id) VALUES(curdate(),curtime(),?)};
	
	my $sthins = $dbh->prepare($ins_statement)
  or die $dbh->errstr;
  
  $sthins->execute($userID)
  or die $sthins->errstr;
 }
# return JSON string
#print $cgi->header(-type => "application/json", -charset => "utf-8");
print $json;