**Clock in**
==================

This post shows how to implement a very simple login function for your website.

The implementation has been divided into the following sections:

The SQL section defines. How to create a table to store the user's information in the database.

The HTML section implements CSS and JavaScript references, as well as the login form
and the session registration form.

The JavaScript section handles the Ajax part using jQuery and JSON.

In the Perl section, it provides the interaction between the user input and the database.

Instructions
------------
1.  Create the user table with the SQL code (change mydb to the appropriate value) and replenish it with some test data.

    ***Note:** you can import the mysql bd that appears in the folder ( \BD\Mysql_BD)* 

2.  Copy and paste the project folder into the htdocs folder of your web server.
    The name of the folder may vary depending on the configuration of your web server.

3. Verify that you have the complete perl libraries 

4. Change the first line of the Perl script according to your version:
   #! / Usr / bin / perl for Unix and #! C: \ Strawberry \ perl \ bin \ perl.exe -wT for Windows) 

5. Enter your domain in the browser http: //localhost/login.html 

5. Enter correct and incorrect values ​​such as username and password to verify the various results. 

Software 
-------------

MySql,
Xampp,
Strawberry Perl,
Notepad ++